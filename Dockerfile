FROM --platform=linux/amd64 debian:bookworm-slim

# Install packages needed for ruby-build
RUN apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
  build-essential openssl \
  ca-certificates \
  curl \
  git \
  libz-dev \ 
  libvips42 libvips-dev libgif-dev \
  automake libtool pkg-config libffi-dev libssl-dev libgmp-dev python3-dev \
  libpq-dev postgresql-client

# Check out ruby-build and install
RUN git clone https://github.com/rbenv/ruby-build.git /usr/local/ruby-build
RUN /usr/local/ruby-build/install.sh

# Install the version of Ruby we want and add to the path
RUN ruby-build 2.7.6 /usr/local/ruby-2.7.6
ENV PATH="/usr/local/ruby-2.7.6/bin:${PATH}"
